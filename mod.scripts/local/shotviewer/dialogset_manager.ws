// ----------------------------------------------------------------------------
struct SSbUiPlacementPreset {
    var id: String;
    var caption: String;
    var slot: array<SStoryBoardPlacementSettings>;
}
// ----------------------------------------------------------------------------
struct SSbUiCameraPreset {
    var id: String;
    var cat1: String;
    var cat2: String;
    var cat3: String;
    var dialogsetId: String;
    var cameraName: String;
    var pos: Vector;
    var rot: EulerAngles;
    var fov: float;
    var zoom: float;
    var dofFocusDistFar: float;
    var dofBlurDistFar: float;
    var dofIntensity: float;
    var dofFocusDistNear: float;
    var dofBlurDistNear: float;
    // not previewable?
    var dofFocalLength: float;
    var dofDistance: float;

    var dofPlane: string;
    var dofTags: String;
}
// ----------------------------------------------------------------------------
abstract class CModSbUiDialogsetPresetsList extends CModUiFilteredList {
    // ------------------------------------------------------------------------
    protected function parsePlacement(pos: String, rot: String) : SStoryBoardPlacementSettings {
        var left, right: String;
        var x, y, z, pitch, yaw, roll: float;
        // "[0.00,0.00,0.00]", "[0.0,0.0,0.0]""

        pos = StrReplace(pos, "[", "");
        pos = StrReplace(pos, "]", "");
        rot = StrReplace(rot, "[", "");
        rot = StrReplace(rot, "]", "");

        StrSplitFirst(pos, ",", left, right);
        x = StringToFloat(left);
        StrSplitFirst(right, ",", left, right);
        y = StringToFloat(left);
        z = StringToFloat(right);

        StrSplitFirst(rot, ",", left, right);
        pitch = StringToFloat(left);
        StrSplitFirst(right, ",", left, right);
        roll = StringToFloat(left);
        yaw = StringToFloat(right);

        return SStoryBoardPlacementSettings(
            Vector(x, y, z),
            EulerAngles(pitch, yaw, roll)
        );
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CModSbUiPlacementPresetsList extends CModSbUiDialogsetPresetsList {
    private var placementPresets: array<SSbUiPlacementPreset>;
    // ------------------------------------------------------------------------
    public function loadCsv(path: String) {
        var data: C2dArray;
        var i: int;
        var presetId: String;
        var currentPreset: SSbUiPlacementPreset;

        data = LoadCSV(path);

        items.Clear();

        // first entry of list is defined as custom placement (id == 0)!
        items.PushBack(SModUiCategorizedListItem(0, GetLocStringByKeyExt("SBUI_ListCustomPlacement")));
        placementPresets.PushBack(SSbUiPlacementPreset("0"));
        currentPreset = SSbUiPlacementPreset("-", GetLocStringByKeyExt("SBUI_ListCustomPlacement"));

        // csv: actors;cat1;cat2;cat3;id;caption;pos;rot;slot
        for (i = 0; i < data.GetNumRows(); i += 1) {
            presetId = data.GetValueAt(4, i);

            if (presetId != currentPreset.id) {
                if (currentPreset.id != "-") {
                    // store preset placements before creating asembling new one
                    placementPresets.PushBack(currentPreset);
                }
                currentPreset = SSbUiPlacementPreset(presetId, data.GetValueAt(5, i));
                currentPreset.slot.Clear();

                items.PushBack(SModUiCategorizedListItem(
                    presetId,
                    data.GetValueAt(5, i),
                    data.GetValueAt(1, i),
                    data.GetValueAt(2, i),
                    data.GetValueAt(3, i)
                ));
            }
            currentPreset.slot.PushBack(
                parsePlacement(data.GetValueAt(6, i), data.GetValueAt(7, i)));
        }

        if (currentPreset.id != "-") {
            placementPresets.PushBack(currentPreset);
        }
    }
    // ------------------------------------------------------------------------
    public function getPlacementsForPreset(id: String) : SSbUiPlacementPreset {
        var i, s: int;

        s = placementPresets.Size();
        // not pretty...
        for (i = 0; i < s; i += 1) {
            if (placementPresets[i].id == id) {
                return placementPresets[i];
            }
        }
        return placementPresets[0];
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CModSbUiCameraPresetsList extends CModSbUiDialogsetPresetsList {
    private var cameraPresets: array<SSbUiCameraPreset>;
    // ------------------------------------------------------------------------
    private function parseTags(str: String) : String {
        // [ext:c:blend]
        // [Notags]
        if (str != "[Notags]") {
            return StrReplaceAll(str, ":", ",");
        } else {
            return "";
        }
    }
    // ------------------------------------------------------------------------
    public function loadCsv(path: String) {
        var data: C2dArray;
        var i: int;
        var placement: SStoryBoardPlacementSettings;

        data = LoadCSV(path);

        items.Clear();
        // csv: cat1;cat2;cat3;dialogset;cameraName;pos;rot;fov;zoom;
        //      dofFocusDistFar;dofBlurDistFar;dofIntensity;dofFocusDistNear;dofBlurDistNear;
        //      dofFocalLength;dofDistance;dofPlane;dofTags;
        for (i = 0; i < data.GetNumRows(); i += 1) {
            placement = this.parsePlacement(data.GetValueAt(5, i), data.GetValueAt(6, i));

            cameraPresets.PushBack(SSbUiCameraPreset(
                i + 1,
                data.GetValueAt(0, i),
                data.GetValueAt(1, i),
                data.GetValueAt(2, i),

                data.GetValueAt(3, i),
                data.GetValueAt(4, i),
                placement.pos,
                placement.rot,
                StringToFloat(data.GetValueAt(7, i)),
                StringToFloat(data.GetValueAt(8, i)),
                StringToFloat(data.GetValueAt(9, i)),
                StringToFloat(data.GetValueAt(10, i)),
                StringToFloat(data.GetValueAt(11, i)),
                StringToFloat(data.GetValueAt(12, i)),
                StringToFloat(data.GetValueAt(13, i)),
                StringToFloat(data.GetValueAt(14, i)),
                StringToFloat(data.GetValueAt(15, i)),
                data.GetValueAt(16, i),
                parseTags(data.GetValueAt(17, i))
            ));
        }
    }
    // ------------------------------------------------------------------------
    public function applyFilter(dialogsetId: String) : CModSbUiCameraPresetsList {
        var i: int;

        items.Clear();

        // first entry of list is defined as custom camera placement (id == 0)!
        items.PushBack(SModUiCategorizedListItem(0, GetLocStringByKeyExt("SBUI_ListCustomCamera")));

        for (i = 0; i < cameraPresets.Size(); i += 1) {
            if (cameraPresets[i].dialogsetId == dialogsetId) {
                items.PushBack(
                    SModUiCategorizedListItem(
                        cameraPresets[i].id,
                        cameraPresets[i].cameraName,
                        cameraPresets[i].cat1,
                        cameraPresets[i].cat2,
                        cameraPresets[i].cat3,
                    )
                );
            }
        }
        return this;
    }
    // ------------------------------------------------------------------------
    public function getCameraSettingsForPreset(camPresetId: String) : SSbUiCameraPreset {
        var i, s: int;

        s = cameraPresets.Size();
        for (i = 0; i < s; i += 1) {
            if (cameraPresets[i].id == camPresetId) {
                return cameraPresets[i];
            }
        }
        return cameraPresets[0];
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CModStoryBoardDialogsetPresetsListsManager {
    // ------------------------------------------------------------------------
    private var placementPresets: CModSbUiPlacementPresetsList;
    private var cameraPresets: CModSbUiCameraPresetsList;
    private var dataLoaded: Bool;
    // ------------------------------------------------------------------------
    public function init() { }
    // ------------------------------------------------------------------------
    private function lazyLoad() {
        placementPresets = new CModSbUiPlacementPresetsList in this;
        placementPresets.loadCsv("dlc\storyboardui\data\dialogset_placement.csv");

        cameraPresets = new CModSbUiCameraPresetsList in this;
        cameraPresets.loadCsv("dlc\storyboardui\data\dialogset_cameras.csv");

        dataLoaded = true;
    }
    // ------------------------------------------------------------------------
    public function activate() {}
    // ------------------------------------------------------------------------
    public function deactivate() {}
    // ------------------------------------------------------------------------
    public function getPlacementPresetsList(actorCount: int) : CModSbUiPlacementPresetsList {
        if (!dataLoaded) { lazyLoad(); }

        return placementPresets;
    }
    // ------------------------------------------------------------------------
    public function getCameraPresetsList(dialogsetId: String) : CModSbUiCameraPresetsList {
        if (!dataLoaded) { lazyLoad(); }

        return cameraPresets.applyFilter(dialogsetId);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
