// ----------------------------------------------------------------------------
state SbUi_CameraSettingSelection in CModStoryBoardCameraMode extends SbUi_FilteredListSelect {
    // ------------------------------------------------------------------------
    // alias
    private var camParams: CModStoryBoardCameraParams;

    private var selectedSetting: SSBUI_CamSetting;
    private var stepSize: float; default stepSize = 0.1;
    // ------------------------------------------------------------------------
    private var isStepSizeChange: bool;
    private var isValueEditing: bool;
    private var isDirectEditOnly: bool;

    private var editedValueType: ESBUI_ValueType;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        this.camParams = parent.cameraParams;
        this.listProvider = parent.cameraSettingsList;

        listProvider.resetWildcardFilter();
        parent.view.listMenuRef.resetEditField();

        updateCamSettingsCaption();
        OnSelected(((CModSbUiCamSettingsList)listProvider).getSelectedId());

        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    event OnIsUiShown() {
        return parent.view.listMenuRef;
    }
    // ------------------------------------------------------------------------
    event OnShowUi(showUi: bool) {
        if (showUi) {
            if (!parent.view.listMenuRef) {
                theGame.RequestMenu('ListView', parent.view);
            }
        } else {
            if (parent.view.listMenuRef) {
                parent.view.listMenuRef.close();
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectPrev'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectNext'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ChangeValue'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleFast', "SBUI_ToggleFastSetting"));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleSlow', "SBUI_ToggleSlowSetting"));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_EditCurrentValue'));

        parent.OnHotkeyHelp(hotkeyList);
    }
    // ------------------------------------------------------------------------
    event OnFilter(action: SInputAction) {}
    // ------------------------------------------------------------------------
    // -- called by listview
    event OnInputCancel() {
        parent.notice(GetLocStringByKeyExt("SBUI_iEditCanceled"));

        isValueEditing = false;
        parent.view.listMenuRef.resetEditField();
        updateView();
    }
    // ------------------------------------------------------------------------
    // -- called by listview
    event OnInputEnd(inputString: String) {
        if (inputString == "") {
            OnResetFilter();
        } else {
            if (isValueEditing) {
                parseAndSetValue(inputString);
                parent.view.listMenuRef.resetEditField();
            } else {
                // Note: filter field is not removed to indicate the current filter
                listProvider.setWildcardFilter(inputString);
            }
            updateView();
        }
        isValueEditing = false;
    }
    // ------------------------------------------------------------------------
    // -- called by listview
    event OnSelected(listItemId: String) {
        // listprovider opens a category if a category was selected otherwise
        // returns true (meaning a "real" item was selected)
        if (listProvider.setSelection(listItemId, true)) {
            selectedSetting = ((CModSbUiCamSettingsList)listProvider).getMeta(
                StringToInt(listItemId));

            isDirectEditOnly = selectedSetting.stepSize == -1
                && selectedSetting.min == -1
                && selectedSetting.max == -1;

            if (selectedSetting.stepSize == 0) {
                stepSize = 0.5;
            } else {
                stepSize = selectedSetting.stepSize;
            }

            //only one of the following in interactively changeable:
            //  fov, zoom, dof intensity, blur N/F, focus N/F
            switch (selectedSetting.sid) {
                case "fov":
                case "zoom":
                case "dofIntensity":
                case "dofFocusFar":
                case "dofFocusNear":
                case "dofBlurFar":
                case "dofBlurNear":
                    parent.selectedSetting = selectedSetting;
                    break;
            }

            parent.notice(GetLocStringByKeyExt("SBUI_iSelectedCameraSetting")
                + selectedSetting.caption);
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    event OnChangeSpeed(action: SInputAction) {
        var settingsStepSize: float;

        if (selectedSetting.stepSize == 0) {
            settingsStepSize = 0.5;
        } else {
            settingsStepSize = selectedSetting.stepSize;
        }

        if (IsPressed(action)) {
            if (action.aName == 'SBUI_ToggleFast') {
                stepSize = settingsStepSize * 4;
            } else {
                stepSize = settingsStepSize / 4;
            }
        } else if (IsReleased(action)) {
            stepSize = settingsStepSize;
        }
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    // value adjustments
    event OnChangeValue(action: SInputAction) {
        var v: SSBUI_Value;

        if (action.value != 0) {

            if (isDirectEditOnly) {
                parent.error(GetLocStringByKeyExt("SBUI_eOnlyDirectSelection"));
            } else {
                v = camParams.adjustValue(
                    selectedSetting.sid, action.value * stepSize,
                    selectedSetting.min, selectedSetting.max);

                updateCurrentValueCaption();
            }
        }
    }
    // ------------------------------------------------------------------------
    private function updateCurrentValueCaption() {
        parent.view.listMenuRef.setExtraField(
                camParams.getFormattedValue(selectedSetting.sid), "#aaaaaa");
    }
    // ------------------------------------------------------------------------
    private function strToFloat(str: String, out f: float) : bool {
        f = StringToFloat(str, -666.6);
        return f != -666.6;
    }
    // ------------------------------------------------------------------------
    private function strToInt(str: String, out i: int) : bool {
        i = StringToInt(str, -666);
        return i != -666;
    }
    // ------------------------------------------------------------------------
    private function strToVector(str: String, out v: Vector) : bool {
        var success: bool;
        var x, y, z: float;
        var remaining, s1, s2, s3: String;

        success = StrSplitFirst(str, " ", s1, remaining);
        success = success && StrSplitFirst(remaining, " ", s2, s3);

        success = success && strToFloat(s1, x);
        success = success && strToFloat(s2, y);
        success = success && strToFloat(s3, z);

        v.X = x;
        v.Y = y;
        v.Z = z;
        v.W = 1.0;

        return success;
    }
    // ------------------------------------------------------------------------
    private function parseValueStr(
        str: String, type: ESBUI_ValueType, out result: SSBUI_Value) : bool
    {
        var success: bool;

        switch (type) {
            case ESBUI_VECTOR:
                success = strToVector(str, result.vec);
                break;

            case ESBUI_FLOAT:
                success = strToFloat(str, result.f);
                break;

            case ESBUI_INT:
                success = strToInt(str, result.i);
                break;
        }
        return success;
    }
    // ------------------------------------------------------------------------
    private function parseAndSetValue(inputString: String) {
        var newValue: SSBUI_Value;

        if (parseValueStr(inputString, editedValueType, newValue)) {
            camParams.setCurrentValue(selectedSetting.sid, newValue);
        } else {
            parent.error(GetLocStringByKeyExt("SBUI_eValueParseErr"));
        }
    }
    // ------------------------------------------------------------------------
    event OnEditValue(action: SInputAction) {
        var strValue: String;
        var value: SSBUI_Value;
        var null: SSBUI_CamSetting;
        var listSetting: CSbUiListSetting;

        if (IsPressed(action)
            && selectedSetting != null
            && !parent.view.listMenuRef.isEditActive())
        {
            value = camParams.getCurrentValue(selectedSetting.sid);

            switch (value.type) {
                case ESBUI_VECTOR:
                    strValue = NoTrailZeros(value.vec.X)
                        + " " + NoTrailZeros(value.vec.Y)
                        + " " + NoTrailZeros(value.vec.Z);
                    break;

                case ESBUI_FLOAT:    strValue = NoTrailZeros(value.f); break;
                case ESBUI_INT:      strValue = IntToString(value.i); break;
                case ESBUI_STRING:
                    switch (selectedSetting.sid) {
                        case "dofAperture":
                            listSetting = new CSbUiDofApertureCamSetting in parent;
                            break;

                        case "dofPlane":
                            listSetting = new CSbUiDofPlaneCamSetting in parent;
                            break;
                    }
                    listSetting.init(selectedSetting, value);

                    parent.selectedListSetting = listSetting;
                    if (listSetting) {
                        parent.PushState(listSetting.getWorkmodeState());
                        return true;
                    } else {
                        return false;
                    }
                    break;

            }
            isValueEditing = true;
            editedValueType = value.type;

            parent.view.listMenuRef.setExtraField("");
            parent.view.listMenuRef.startInputMode(
                GetLocStringByKeyExt("SBUI_lEditValue"), strValue);
        }
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    protected function updateView() {
        updateCurrentValueCaption();

        // set updated list data and render in listview
        parent.view.listMenuRef.setListData(
            listProvider.getFilteredList(),
            listProvider.getMatchingItemCount(),
            // number of items without filtering
            listProvider.getTotalCount());

        parent.view.listMenuRef.updateView();
    }
    // ------------------------------------------------------------------------
    private function updateCamSettingsCaption() {
        parent.view.title = GetLocStringByKeyExt("SBUI_SelectCameraSettingsModeName");
        parent.view.statsLabel = GetLocStringByKeyExt("SBUI_SelectCameraSettingsListTitle");
        // update field if the menu is already open
        parent.view.listMenuRef.setTitle(parent.view.title);
        parent.view.listMenuRef.setStatsLabel(parent.view.statsLabel);
    }
    // ------------------------------------------------------------------------
    private function registerListeners() {
        super.registerListeners();

        theInput.RegisterListener(this, 'OnCycleSelection', 'SBUI_SelectPrev');
        theInput.RegisterListener(this, 'OnCycleSelection', 'SBUI_SelectNext');

        theInput.RegisterListener(this, 'OnChangeValue', 'SBUI_ChangeValue');
        theInput.RegisterListener(this, 'OnEditValue', 'SBUI_EditCurrentValue');
    }
    // ------------------------------------------------------------------------
    private function unregisterListeners() {
        super.unregisterListeners();

        theInput.UnregisterListener(this, 'SBUI_SelectPrev');
        theInput.UnregisterListener(this, 'SBUI_SelectNext');

        theInput.UnregisterListener(this, 'SBUI_ChangeValue');
        theInput.UnregisterListener(this, 'SBUI_EditCurrentValue');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
state SbUi_DofPlaneSelection in CModStoryBoardCameraMode extends SbUi_GenericListSelection {
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        editedSetting = parent.selectedListSetting;
        super.setupLabels("DofPlane", editedSetting.getCaption());

        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state SbUi_DofApertureSelection in CModStoryBoardCameraMode extends SbUi_GenericListSelection {
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        editedSetting = parent.selectedListSetting;
        super.setupLabels("DofAperture", editedSetting.getCaption());

        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    protected function setListSelection() {
        //FIXME value id is string
        ((CSbUiGenericSettingList)listProvider).setOrAddSelection(editedSetting.getValueId(), true);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
abstract state SbUi_GenericListSelection in CModStoryBoardCameraMode
    extends SbUi_FilteredListSelect
{
    // ------------------------------------------------------------------------
    protected var editedSetting: CSbUiListSetting;
    protected var selectedValue: String;

    //protected var listProvider: CSbUiGenericSettingList;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        listProvider = parent.getListProvider(editedSetting.getValueListId());
        this.setListSelection();

        parent.view.listMenuRef.resetEditField();
        OnUpdateView();
        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    protected function setListSelection() {
        listProvider.setSelection(editedSetting.getValueId(), true);
    }
    // ------------------------------------------------------------------------
    protected function setupLabels(type: String, itemName: String) {
        parent.view.title = GetLocStringByKeyExt("SBUI_" + type + "Title") + " " + itemName;
        parent.view.statsLabel = GetLocStringByKeyExt("SBUI_" + type + "ListElements");

        // update fields if the menu is already open
        parent.view.listMenuRef.setTitle(parent.view.title);
        parent.view.listMenuRef.setStatsLabel(parent.view.statsLabel);
        parent.view.listMenuRef.setExtraField("");
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        parent.OnHotkeyHelp(hotkeyList);
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectPrev', "SBUI_SelectPrevSetting"));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectNext', "SBUI_SelectNextSetting"));
    }
    // ------------------------------------------------------------------------
    //protected function onListSelection(selectedId: String);
    // ------------------------------------------------------------------------
    event OnSelected(selectedId: String) {
        if (listProvider.setSelection(selectedId, true)) {
            editedSetting.setValueId(selectedId);
            parent.cameraParams.setCurrentValue(
                editedSetting.getId(),
                editedSetting.getValue());
        }
        OnUpdateView();
    }
    // ------------------------------------------------------------------------
    event OnUpdateView() {
        updateCurrentValueCaption();
        updateView();
    }
    // ------------------------------------------------------------------------
    private function updateCurrentValueCaption() {
        parent.view.listMenuRef.setExtraField(
                parent.cameraParams.getFormattedValue(editedSetting.getId()), "#aaaaaa");
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        theInput.RegisterListener(this, 'OnCycleSelection', 'SBUI_SelectPrev');
        theInput.RegisterListener(this, 'OnCycleSelection', 'SBUI_SelectNext');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        theInput.UnregisterListener(this, 'SBUI_SelectPrev');
        theInput.UnregisterListener(this, 'SBUI_SelectNext');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
