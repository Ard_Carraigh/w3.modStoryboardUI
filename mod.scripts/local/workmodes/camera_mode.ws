// -----------------------------------------------------------------------------
//
// BUGS:
//
// TODO:
//  - add hotkey for camera settings info (relative to origin from placementDirector)
//
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
class CStoryBoardInteractiveCamera extends CStoryBoardShotCamera {
    private var isInteractiveMode: bool;

    private var stepMoveSize: float; default stepMoveSize = 0.10;
    private var stepRotSize: float; default stepRotSize = 0.10;
    private var stepPadSize: float; default stepPadSize = 20.0;

    private var defaultDofCenterRadius: float; default defaultDofCenterRadius = 0.5;
    // ------------------------------------------------------------------------
    public function startInteractiveMode() {
        if (!isInteractiveMode) {
            // repeats & overrideExisting = true
            AddTimer('updateInteractiveSettings', 0.015f, true, , , , true);
            isInteractiveMode = true;
        }
    }
    // ------------------------------------------------------------------------
    public function stopInteractiveMode() {
        RemoveTimer('updateInteractiveSettings');
        isInteractiveMode = false;
    }
    // ------------------------------------------------------------------------
    timer function updateInteractiveSettings(deltaTime: float, id: int) {
        var newPos: Vector;
        var newRot: EulerAngles;
        var directionFB, directionLR: float;
        var moveFB, moveLR, moveUD: float;
        var rotYaw, rotPitch, rotYawPad, rotPitchPad: float;

        moveFB = theInput.GetActionValue('SBUI_MoveForwardBack');
        moveLR = theInput.GetActionValue('SBUI_MoveLeftRight');
        moveUD = theInput.GetActionValue('SBUI_MoveUpDown');

        rotYaw = theInput.GetActionValue('GI_MouseDampX');
        rotPitch = theInput.GetActionValue('GI_MouseDampY');

        // Gamepad Support
        rotYawPad = stepPadSize * theInput.GetActionValue('SBUI_AxisRightX');
        rotPitchPad = -stepPadSize * theInput.GetActionValue('SBUI_AxisRightY');

        if (moveFB != 0 || moveLR != 0 || moveUD != 0 || rotYaw != 0 || rotPitch != 0 || rotYawPad != 0 || rotPitchPad != 0)
        {
            newPos = settings.pos;
            newRot = settings.rot;

            directionFB = Deg2Rad(GetHeading() + 90);
            directionLR = Deg2Rad(GetHeading());

            newPos.X += moveFB * stepMoveSize * CosF(directionFB)
                      + moveLR * stepMoveSize * CosF(directionLR);

            newPos.Y += moveFB * stepMoveSize * SinF(directionFB)
                      + moveLR * stepMoveSize * SinF(directionLR);

            newPos.Z += moveUD * stepMoveSize;

            newRot.Yaw -= (rotYaw + rotYawPad) * stepRotSize;
            newRot.Pitch -= (rotPitch + rotPitchPad) * stepRotSize;

            if (newPos != settings.pos || newRot != settings.rot) {
                settings.pos = newPos;
                settings.rot = newRot;

                this.TeleportWithRotation(settings.pos, settings.rot);
            }
        }
    }
    // ------------------------------------------------------------------------
    public function adjustFov(step: float) {
        this.setFov(settings.fov + step);
    }
    // ------------------------------------------------------------------------
    public function adjustDofStrength(step: float) {
        settings.dof.strength = settings.dof.strength + step;
        // clamping will be applied in setDof
        this.setDof(settings.dof);
        settings.dof = getDof();
    }
    // ------------------------------------------------------------------------
    public function adjustDofBlur(isNear: bool, step: float) {
        if (isNear) {
            settings.dof.blurNear = settings.dof.blurNear + step;
        } else {
            settings.dof.blurFar = settings.dof.blurFar + step;
        }
        // clamping will be applied in setDof
        this.setDof(settings.dof);
        settings.dof = getDof();
    }
    // ------------------------------------------------------------------------
    public function adjustDofFocus(isNear: bool, step: float) {
        if (isNear) {
            settings.dof.focusNear = settings.dof.focusNear + step;
        } else {
            settings.dof.focusFar = settings.dof.focusFar + step;
        }
        // clamping will be applied in setDof
        this.setDof(settings.dof);
        settings.dof = getDof();
    }
    // ------------------------------------------------------------------------
    public function adjustDofRadius(step: float) {
        var radius: float;
        var near, far: float;

        radius = (settings.dof.blurFar - settings.dof.blurNear) / 2.0;
        near = settings.dof.blurNear -= step;
        far = settings.dof.blurFar += step;

        if (near < far) {
            settings.dof.blurNear = near;
            settings.dof.blurFar = far;
        } else {
            settings.dof.blurNear = near + radius;
            settings.dof.blurFar = settings.dof.blurNear;
        }

        radius = (settings.dof.focusNear - settings.dof.focusNear) / 2.0;
        near = settings.dof.focusNear -= step;
        far = settings.dof.focusFar += step;

        if (near < far) {
            settings.dof.focusNear = near;
            settings.dof.focusFar = far;
        } else {
            settings.dof.focusNear = near + radius;
            settings.dof.focusFar = settings.dof.focusNear;
        }

        this.setDof(settings.dof);
        settings.dof = getDof();
    }
    // ------------------------------------------------------------------------
    public function setDofCenterPoint(p: Vector) {
        var dist: float = VecDistance(settings.pos, p);
        var radiusBlur, radiusFocus: float;

        // Note: this is a total hack. correct calculation requires more effort...
        // http://www.dofmaster.com/articles.html
        // and probably cannot be previewed anyway...

        radiusBlur = defaultDofCenterRadius * 2.0;
        radiusFocus = defaultDofCenterRadius;

        settings.dof.blurFar = dist + radiusBlur * 2.0;
        settings.dof.blurNear = dist - radiusBlur;

        settings.dof.focusFar = dist + radiusFocus;
        settings.dof.focusNear = dist - radiusFocus;

        this.setDof(settings.dof);

        settings.dof = getDof();
    }
    // ------------------------------------------------------------------------
    public function getDofSettings() : SStoryBoardCameraDofSettings {
        return getDof();
    }
    // ------------------------------------------------------------------------
    public function setStepSize(move: float, rot: float) {
        stepMoveSize = move;
        stepRotSize = rot;
    }
    // ------------------------------------------------------------------------
    public function getStepSize(out moveStep: float, out rotStep: float) {
        moveStep = stepMoveSize;
        rotStep = stepRotSize;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state SbUi_CameraPresetSelection in CModStoryBoardCameraMode
    extends SbUi_FilteredListSelect
{
    // ------------------------------------------------------------------------
    // alias
    private var theCam: CStoryBoardInteractiveCamera;
    private var customCameraSettings: SStoryBoardCameraSettings;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        var i, actorCount : int;
        var preset: SSbUiPlacementPreset;

        theCam = parent.theCam;

        preset = parent.dialogsetPresetsListsManager.getPlacementPresetsList(-1)
            .getPlacementsForPreset(parent.shot.getDialogsetId());

        parent.view.title =
            GetLocStringByKeyExt(parent.workMode + "PresetsName")
                + " (<font color=\"#ED8D33\">" + preset.caption + "</font>)";

        parent.view.statsLabel = GetLocStringByKeyExt("SBUI_SelectCameraPresetsListTitle");
        // update fields if the menu is already open
        parent.view.listMenuRef.setTitle(parent.view.title);
        parent.view.listMenuRef.setStatsLabel(parent.view.statsLabel);

        //actorAssets = parent.assetManager.getActorAssets();
        // store settings of current camera for reset-to-custom options
        customCameraSettings = theCam.getSettings();

        listProvider = parent.dialogsetPresetsListsManager.getCameraPresetsList(
            parent.shot.getDialogsetId());

        // default selection is "current camera / no preset"
        listProvider.setSelection("0", true);

        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        super.OnHotkeyHelp(hotkeyList);

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectPrev'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectNext'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ChangeCameraSettings'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleUi'));
    }
    // ------------------------------------------------------------------------
    event OnSelected(optionId: String) {
        var camSettings: SStoryBoardCameraSettings;
        var camPreset: SSbUiCameraPreset;

        if (listProvider.setSelection(optionId, true)) {
            if (optionId == "0") {
                resetCamera();
            } else {
                camPreset = ((CModSbUiCameraPresetsList)listProvider).getCameraSettingsForPreset(optionId);
                camSettings = SStoryBoardCameraSettings(
                    VecTransform(
                        MatrixBuiltRotation(EulerAngles(
                            parent.origin.rot.Pitch, parent.origin.rot.Yaw, parent.origin.rot.Roll
                        )),
                        camPreset.pos
                    ) + parent.origin.pos,
                    EulerAngles(
                        AngleNormalize(parent.origin.rot.Pitch + camPreset.rot.Pitch),
                        AngleNormalize(parent.origin.rot.Yaw + camPreset.rot.Yaw),
                        AngleNormalize(parent.origin.rot.Roll + camPreset.rot.Roll)
                    ),

                    camPreset.fov,
                    SStoryBoardCameraDofSettings(
                        camPreset.dofIntensity,
                        camPreset.dofBlurDistNear,
                        camPreset.dofBlurDistFar,
                        camPreset.dofFocusDistNear,
                        camPreset.dofFocusDistFar
                    ),
                    camPreset.zoom,
                    camPreset.dofFocalLength,
                    camPreset.dofDistance,
                    camPreset.dofPlane,
                    camPreset.dofTags
                );
                theCam.setSettings(camSettings);
                theCam.switchTo();

                parent.notice(GetLocStringByKeyExt("SBUI_iSelectedCameraPreset")
                    + camPreset.cameraName);
            }
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    private function resetCamera() {
        theCam.setSettings(customCameraSettings);
        theCam.switchTo();

        listProvider.setSelection("0", true);
        updateView();
    }
    // ------------------------------------------------------------------------
    event OnResetCamera(action: SInputAction) {
        if (IsPressed(action)) {
            resetCamera();
        }
    }
    // ------------------------------------------------------------------------
    event OnIsUiShown() {
        return parent.view.listMenuRef;
    }
    // ------------------------------------------------------------------------
    event OnShowUi(showUi: bool) {
        if (showUi) {
            if (!parent.view.listMenuRef) {
                theGame.RequestMenu('ListView', parent.view);
            }
        } else {
            if (parent.view.listMenuRef) {
                parent.view.listMenuRef.close();
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnShowCameraSettings(action: SInputAction) {
        if (IsPressed(action)) {
            parent.PushState('SbUi_CameraSettingSelection');
        }
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        super.registerListeners();

        theInput.RegisterListener(this, 'OnResetCamera', 'SBUI_ResetCamera');
        theInput.RegisterListener(this, 'OnCycleSelection', 'SBUI_SelectPrev');
        theInput.RegisterListener(this, 'OnCycleSelection', 'SBUI_SelectNext');
        theInput.RegisterListener(this, 'OnShowCameraSettings', 'SBUI_ChangeCameraSettings');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        super.unregisterListeners();

        theInput.UnregisterListener(this, 'SBUI_ResetCamera');
        theInput.UnregisterListener(this, 'SBUI_SelectPrev');
        theInput.UnregisterListener(this, 'SBUI_SelectNext');
        theInput.UnregisterListener(this, 'SBUI_ChangeCameraSettings');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// rootstate == OnBack event triggers change to Overview Mode
state SbUi_InteractiveCamera in CModStoryBoardCameraMode extends SbUi_WorkModeRootState
{
    // alias
    private var theCam: CStoryBoardInteractiveCamera;
    // current step sizes
    private var moveStepSize: float;
    private var rotStepSize: float;
    private var settingStepSize: float; default settingStepSize = 0.1;

    private var lastDofCenterActorId: String;
    private var areGuidesVisible: bool;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        // start default interactive mode
        theCam = parent.theCam;
        theCam.startInteractiveMode();
        theCam.getStepSize(moveStepSize, rotStepSize);

        registerListeners();

        showCamGuides(true);
        // (any) valid id is required as start
        lastDofCenterActorId = parent.assetManager.getPreviousActorId();

        if (parent.selectedSetting.stepSize == 0) {
            settingStepSize = 0.5;
        } else {
            settingStepSize = parent.selectedSetting.stepSize;
        }
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        theCam.stopInteractiveMode();
        showCamGuides(false);
        unregisterListeners();
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_AcceptChanges', "SBUI_AcceptCamChanges"));

        hotkeyList.PushBack(HotkeyHelp_from('GI_MouseDampX', "SBUI_CamRotYaw"));
        hotkeyList.PushBack(HotkeyHelp_from('GI_MouseDampY', "SBUI_CamRotPitch"));

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_MoveForwardBack'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_MoveLeftRight'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_MoveUpDown'));

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_CamGuides'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectCameraPreset'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ChangeCameraSettings'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_CamFovAdjust'));

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_CycleDofOnAsset'));

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleFast'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleSlow'));
    }
    // ------------------------------------------------------------------------
    event OnToggleGuides(action: SInputAction) {
        if (!IsPressed(action)) {
            showCamGuides(!areGuidesVisible);
        }
    }
    // ------------------------------------------------------------------------
    event OnSelectCameraPreset(action: SInputAction) {
        if (IsPressed(action)) {
            if (parent.shot.getDialogsetId() != "") {
                parent.PushState('SbUi_CameraPresetSelection');
            } else {
                parent.error(GetLocStringByKeyExt("SBUI_eNoDialogsetSelected"));
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnShowCameraSettings(action: SInputAction) {
        if (IsPressed(action)) {
            parent.PushState('SbUi_CameraSettingSelection');
        }
    }
    // ------------------------------------------------------------------------
    private function showCamGuides(doShow: bool) {
        if (doShow) {
            theGame.RequestPopup('TestPopup');
        } else {
            theGame.ClosePopup('TestPopup');
        }
        areGuidesVisible = doShow;
    }
    // ------------------------------------------------------------------------
    private function cycleDofOnAsset() {
        var actor: CModStoryBoardActor;
        var placement: SStoryBoardPlacementSettings;
        var dofCenterActorId: String;

        dofCenterActorId = parent.assetManager.getNextInteractionActorId(
                "-", lastDofCenterActorId);

        if (dofCenterActorId != lastDofCenterActorId) {
            actor = (CModStoryBoardActor)parent.assetManager.getAsset(dofCenterActorId);

            placement = actor.getCurrentPlacement();

            theCam.setDofCenterPoint(placement.pos);

            lastDofCenterActorId = dofCenterActorId;

            // info
            parent.notice(GetLocStringByKeyExt("SBUI_iCamDofActor")
                + " " + actor.getName());
        }
    }
    // ------------------------------------------------------------------------
    event OnChangeDof(action: SInputAction) {
        if (IsPressed(action)) {
            this.cycleDofOnAsset();
        }
    }
    // ------------------------------------------------------------------------
    event OnChangeFov(action: SInputAction) {
        if (action.value != 0) {
            // value is -1 or 1 (as specified in input settings!)
            theCam.adjustFov(action.value * settingStepSize / 4.0);

            parent.notice(GetLocStringByKeyExt("SBUI_iCamFov")
                + FloatToString(theCam.GetFov()));
        }
    }
    // ------------------------------------------------------------------------
    // value adjustments
    event OnChangeValue(action: SInputAction) {
        var v: SSBUI_Value;

        if (action.value != 0) {
            v = parent.cameraParams.adjustValue(
                parent.selectedSetting.sid, action.value * settingStepSize,
                parent.selectedSetting.min, parent.selectedSetting.max);

            parent.notice(parent.selectedSetting.caption + " "
                + parent.cameraParams.getFormattedValue(parent.selectedSetting.sid));
        }
    }
    // ------------------------------------------------------------------------
    event OnChangeSpeed(action: SInputAction) {
        var stepSize: float;

        if (parent.selectedSetting.stepSize == 0) {
            stepSize = 0.5;
        } else {
            stepSize = parent.selectedSetting.stepSize;
        }

        if (IsPressed(action)) {
            if (action.aName == 'SBUI_ToggleFast') {
                // --- fast ---
                settingStepSize = stepSize * 4;
                moveStepSize = 0.40;
                rotStepSize = 0.20;
                parent.theCam.setStepSize(moveStepSize, rotStepSize);
            } else {
                // --- slow ---
                settingStepSize = stepSize / 4;
                moveStepSize = 0.025;
                rotStepSize = 0.025;
                parent.theCam.setStepSize(moveStepSize, rotStepSize);
            }
        } else if (IsReleased(action)) {
            // -- normal ---
            settingStepSize = stepSize;
            moveStepSize = 0.10;
            rotStepSize = 0.10;
            parent.theCam.setStepSize(moveStepSize, rotStepSize);
        }
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        theInput.RegisterListener(this, 'OnToggleGuides', 'SBUI_CamGuides');

        theInput.RegisterListener(this, 'OnChangeDof', 'SBUI_CycleDofOnAsset');
        theInput.RegisterListener(this, 'OnChangeFov', 'SBUI_CamFovAdjust');
        theInput.RegisterListener(this, 'OnChangeValue', 'SBUI_ChangeValue');

        theInput.RegisterListener(this, 'OnShowCameraSettings', 'SBUI_ChangeCameraSettings');
        theInput.RegisterListener(this, 'OnSelectCameraPreset', 'SBUI_SelectCameraPreset');
        theInput.RegisterListener(this, 'OnBack', 'SBUI_AcceptChanges');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        theInput.UnregisterListener(this, 'SBUI_CamGuides');

        theInput.UnregisterListener(this, 'SBUI_CycleDofOnAsset');
        theInput.UnregisterListener(this, 'SBUI_CamFovAdjust');
        theInput.UnregisterListener(this, 'SBUI_ChangeValue');

        theInput.UnregisterListener(this, 'SBUI_ChangeCameraSettings');
        theInput.UnregisterListener(this, 'SBUI_SelectCameraPreset');
        theInput.UnregisterListener(this, 'SBUI_AcceptChanges');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
statemachine class CModStoryBoardCameraMode extends CModSbListViewWorkMode {
    default workMode = 'SBUI_ModeCamera';
    default workContext = 'MOD_StoryBoardUi_ModeCamera';
    default generalHelpKey = "SBUI_CameraGeneralHelp";
    // manages the set of dialogset placment presets to choose from
    protected var dialogsetPresetsListsManager: CModStoryBoardDialogsetPresetsListsManager;
    // ------------------------------------------------------------------------
    protected var theCam: CStoryBoardInteractiveCamera;
    protected var cameraParams: CModStoryBoardCameraParams;
    protected var cameraSettingsList: CModSbUiCamSettingsList;
    protected var assetManager: CModStoryBoardAssetManager;
    protected var origin: SStoryBoardOriginStateData;
    // ------------------------------------------------------------------------
    // this setting will be used for generic value changes
    protected var selectedSetting: SSBUI_CamSetting;
    // sublist settings, stored in parent os it is accessible in substate
    protected var selectedListSetting: CSbUiListSetting;
    protected var genericListProvider: CSbUiSettingListProvider;
    // ------------------------------------------------------------------------
    public function init(storyboard: CModStoryBoard) {
        super.init(storyboard);
        this.assetManager = storyboard.getAssetManager();
        this.dialogsetPresetsListsManager = storyboard.getDialogsetPresetsListsManager();
        this.origin = storyboard.getOrigin();

        this.cameraParams = new CModStoryBoardCameraParams in this;
        this.cameraSettingsList = new CModSbUiCamSettingsList in this;
        this.genericListProvider = new CSbUiSettingListProvider in this;

        this.cameraSettingsList.initList();
    }
    // ------------------------------------------------------------------------
    private function createInteractiveCam() : CStoryBoardInteractiveCamera {
        var ent: CEntity;
        var template: CEntityTemplate;

        template = (CEntityTemplate)LoadResource("dlc\modtemplates\storyboardui\interactive_camera.w2ent", true);
        ent = theGame.CreateEntity(template,
            thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        return (CStoryBoardInteractiveCamera)ent;
    }
    // ------------------------------------------------------------------------
    event OnIsUiShown() { return false; }
    event OnShowUi(showUi: bool) { }
    // ------------------------------------------------------------------------
    public function isUiShown() : bool { return OnIsUiShown(); }
    // ------------------------------------------------------------------------
    public function showUi(showUi: bool) { OnShowUi(showUi); }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        super.registerListeners();

        theInput.RegisterListener(this, 'OnChangeSpeed', 'SBUI_ToggleFast');
        theInput.RegisterListener(this, 'OnChangeSpeed', 'SBUI_ToggleSlow');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        super.unregisterListeners();

        theInput.UnregisterListener(this, 'SBUI_ToggleFast');
        theInput.UnregisterListener(this, 'SBUI_ToggleSlow');
    }
    // ------------------------------------------------------------------------
    protected function getListProvider(listId: String) : CSbUiGenericSettingList {
        return genericListProvider.getListProvider(listId);
    }
    // ------------------------------------------------------------------------
    public function activate(shot: CModStoryBoardShot) {
        super.activate(shot);

        // create interactive cam and setup shotsettings
        theCam = createInteractiveCam();

        theCam.setSettings(shot.getCameraSettings());
        theCam.activate();

        cameraParams.setCam(theCam);
        // default is fov
        cameraSettingsList.setSelection(cameraSettingsList.getDefaultSettingId(), true);
        selectedSetting = cameraSettingsList.getMeta(cameraSettingsList.getDefaultSettingId());

        PushState('SbUi_InteractiveCamera');
    }
    // ------------------------------------------------------------------------
    public function hasModifiedSettings() : bool {
        //FIXME verify if cam settings differ from start settings
        return true;
    }
    // ------------------------------------------------------------------------
    public function storeSettings() {
        shot.setCameraSettings(theCam.getSettings());
    }
    // ------------------------------------------------------------------------
    public function deactivate() {
        super.deactivate();
        // destroy guides popup if it is still shown
        theGame.ClosePopup('TestPopup');

        // do NOT deactivate this cam cause it will trigger a return to the
        // gamecamera!
        theCam.stopInteractiveMode();
        theCam.Destroy();
        delete theCam;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
