// ----------------------------------------------------------------------------
// Storyboard UI: additional actor animaions can be added dynamically, these
// are the required infos:
struct SSbUiExtraAnimation {
    // unique, numerical id for this animation
    // Note: 100000 will be added to this values so it does not conflict with
    // vanilla animations. this id MUST be unique.
    // Note: see additional info below if the animation has to be usable in
    // radish scene encoder
    var animId: int;
    // name identifying the animation. this is used to start the animation with
    // scripts in sbui.
    // Note: the name should be unique in the set of all animations assigned to
    // an actor template. it does not need necessarily to be unique in the set
    // of all animations in the game (e.g. there are many different walking
    // animations for different skeletons/templates (monsters, animals, man,
    // women, ...) all referenced with the aninName 'walk'. but for every
    // spawned actor there is only one unambigues 'walk' animation).
    var animName: CName;
    // optional sub category 1 to group animations
    var subCategory1: String;
    // optional sub category 2 to group animations
    var subCategory2: String;
    // caption for animation in sbui selection list
    var caption: String;
    // number of frames of this animation
    var frames: int;
}
// ----------------------------------------------------------------------------
// Note: compatibility with radish scene encoder
// ---------------------------------------------
// if custom animations should be usable with radish scene encoder additional
// information must be provided for the radish encoder in a repository file.
// see
//      <radish modding tools>/repos.scenes/sbui.custom_animations.repo.yml
//      <radish modding tools>/repos.scenes/sbui.custom_mimics.repo.yml
// for more information
// ----------------------------------------------------------------------------
// add custom mod animations for actors here:
// ----------------------------------------------------------------------------
function SBUI_getExtraAnimations() : array<SSbUiExtraAnimation> {
    var anims: array<SSbUiExtraAnimation>;

    // Note: order must be sorted by subCategory1, subCategory2,
    //anims.PushBack(SSbUiExtraAnimation(1, 'man_work_sit_table_sleep_stop', "work", "man", "new anim caption", 110));
    //anims.PushBack(SSbUiExtraAnimation(2, 'geralt_reading_book_loop_01', "idle", , "new idle anim caption", 300));
    //anims.PushBack(SSbUiExtraAnimation(321, 'fancy_animname', , , "new fancy anim", 123));

    return anims;
}
// ----------------------------------------------------------------------------
function SBUI_getExtraMimics() : array<SSbUiExtraAnimation> {
    var mimicAnims: array<SSbUiExtraAnimation>;

    // Note: order must be sorted by subCategory1, subCategory2,
    //mimicAnims.PushBack(SSbUiExtraAnimation(1, 'geralt_neutral_gesture_eating_face', "man", , "new mimics caption", 259));
    //mimicAnims.PushBack(SSbUiExtraAnimation(321, 'fancy_mimicanimname', , , "new fancy mimicanim", 123));

    return mimicAnims;
}
// ----------------------------------------------------------------------------
function SBUI_getExtraIdleAnimations() : array<SSbUiExtraAnimation> {
    var idleAnims: array<SSbUiExtraAnimation>;

    // Note: these animations must also be available as "normal" animation with
    // the same ids as above (also in the sbui.custom_animations.repo.yml!)

    // Note: order must be sorted by subCategory1, subCategory2,
    //idleAnims.PushBack(SSbUiExtraAnimation(2, 'geralt_reading_book_loop_01', , , "new idle anim caption", 300));
    //idleAnims.PushBack(SSbUiExtraAnimation(321, 'fancy_animname', , , "new fancy anim", 123));

    return idleAnims;
}
// ----------------------------------------------------------------------------
